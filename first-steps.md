# Ampersand & language guide draft

## Compiler usage

Use `etc --help` to print available compiler options.  All of the examples below can be compiled with the options:

```
etc --system-c -o example example.amp
./example
```

If you don't have clang installed or don't want to use clang, use the following workflow to build.  However, clang is
highly recommended because the following approach may or may not work.

```
etc -o=example example.amp
./example
```

## Hello, &

```c
use extern "C" "stdio.h";

#[no_mangle]
fn main: (argc: sint, argv: **schar) -> sint = {
    let amp = "ampersand &";
    printf("Hello, %.*s\n".ptr, amp.len, amp.ptr);
    return 0;
}
```

Let's break this down line by line.

The first line imports the C header file stdio.h.  This is similar to the directive `#include <stdio.h>` in C.  It
imports all non-static function declarations, extern global variables, struct definitions, union definitions and
typedef's so they can be used from `&`.

```c
use extern "C" "stdio.h";
```

This next two lines declare a function with an attribute.  The function is called main.  Because it is declared as
`no_mangle`, it will also be linked as `main`, so that it can be called by the system libc.  Otherwise the precise
linkage name would be a symbol available only internally from &  (the linkage name would be precisely
`_ZN4mainEN4sintENPP5scharE`).  The function returns an `sint`.  `sint` is a signed (hence the `s`) integer with the
width of precisly 32 bits.  Unlike in C however, the width is well-defined for any platform.  Other signed integer types
include `schar`, `sshort`, `slong`, `sllong`, which are 8, 16, 64 and 128 bits in width accordingly.  Unsigned integer
types are the same, except they use the `u` prefix instead of `s`.  `**schar` is a pointer to a pointer of `schar`.
Pointers declare a location in memory, just like in C.

```c
#[no_mangle]
fn main: (argc: sint, argv: **schar) -> sint
```

The first line in the function body declares and defines a new variable binding with the name `amp`.  `let` allows one
to declare a variable binding without declaring its type.  This is similar to the C++11/C11 `auto` keyword, but somewhat
more powerful.  The initializer `"ampersand &"` is a string.  Unlike in C, `__lang::builtin::String`, or simply `String`
is a builtin record type in `&`, and not just a pointer to an `schar` array.  

```c
    let amp = "ampersand &";
```

If string were defined as a regular struct, it would look like this.

```c
struct String {
    len: usizet,
    ptr: *schar,
}
```

Both fields of the `String` type are used in the next line.  First, we take the `ptr` to the first `schar`.  We can pass
it to the C `printf` function just like we would a C string, because all string literals are nul-terminated.  Next we
take the `len` field of the `amp` binding to tell the `printf` formatting function how many bytes to print.  `len` has
the type `usizet`, which is an unsigned machine-word sized integer --- it is guaranteed to have the same width as a
pointer.  Next we take the `ptr` field to pass to the `printf` function.  Just like in C, all statements are terminated
with a semicolon `;`.

```c
    printf("Hello, %.*s\n".ptr, amp.len, amp.ptr);
```

The last line of the function body returns from the function body.  In `&` all functions, except those returning `void`
must have an explicit `return` statement in every branch.  A return value of `0` from the main function means success on
UNIX-like systems.

## Operators

Almost all operators behave the same as they do in C.  The differencese are listed here.

The `?` operator can have its left-hand side argument ommited.  Addtionaly both arguments can be control flow statements
like `continue`, `break` and `return`.  This allows for some interesting control flow, like for example:

```c
#[no_mangle]
fn main: () -> sint = {
    let result = some_function();
    result ?: return 1;
    return 0;
}
```

The new `..` operator is used for creating ranges of integers.  `0 .. 10` is a range of `0` to `10`.  NOTE: a bug in the
lexer prevents one from using the form `0..10`.  Another bug prevents one to use real numbers with ranges.  The ranges
can be used with the index operator.

The `[]` operator is defined for pointer types, slice types and const array types.  NOTE: indexing const arrays doesn't
work yet.  The two array-like types can additionally be indexed using ranges of integers to create new slices.

```c
let array = [1, 2, 3];
let slice = array[1 .. 3]; // [2, 3]
```

Slices are builtin types with the signature `T[]`, where T is a concrete type.  Slices have two fields: `len` and `ptr`.
`len` is the current length, `ptr` is the pointer to the first element.

Casting also works slightly differently.  The syntax for a cast is as follows:

```c
cast(float, 1.0)
```

Although it looks like a function application, it is actually a special keyword and a special syntax that only accepts
one type argument and one function argument.  The semantics are similar to C: casting a float to integer and vice-versa
acts as a numeric cast, not a bitwise one.  Casts between pointers and integers are only possible through the `usizet`
type.  All integer types can be cast between each other.

## New types

There are 4 kinds of user-definable types.  `struct`s, `union`s, `enum`s and `tagged` unions.  A struct behaves almost
the same as in C.  So does a union.  However, the syntax differs for both of these.  No user-defined type requires a
prefix like `struct Foo` or `union Bar` when used as a function parameter or variable declaration.

```c
struct Struct {
    a: sint,
    b: float,
}
```

Notice the commas instead of semicolons and the lack of a terminating semicolon.

Enums, unlike in C, are strongly typed.  Meaning they can't be used interchangeably with integers.  The definition

```c
enum Enum {
    Foo,
    Bar,
    Baz = 255,
}
```

will create an enumeration with only 3 valid values: `Enum::Foo`, `Enum::Bar` and `Enum::Baz`.  These can however be
converted to and from unsigned integer types using the `cast` operator: `cast(uint, Enum::Foo)`, `cast(Enum, 255u)`.
NOTE: currently casts are not type-checked properly, so to cast an unsigned integer to an enum, the integer might have
to be manually constrained with the `u` postfix.

### Tagged union

The `tagged` union could have a whole chapter written about it.  Instead it will be split over three chapters.  Chapter
1: definition.

```c
tagged Tagged {
    common: bool,
    Integer {
        int: sint,
    },
    Float {
        real: float,
    },
}
```

This definition will define a new `tagged` type with two variants: `Tagged::Integer`, which holds one `sint` under the
name `int`; and the `Tagged::Float` which holds one `float` under the name `real`.  A tagged is used instantiated as one
of its variants: `Tagged::Integer(common: false, int: 42)`. Additionally, every variant of this tagged union has access to the
common `common: bool` field. Internally an unsigned integer is used to differentiate between the two variants.  It can
be accesed through the `.tag` field accessible on any variant.

## Statements

`if .. { ... } else { ... }`, `while .. { ... }`, `do { ... } while ...;` and `for ..; ..; .. { ... }` all
require their body to be enclosed in a compound statement `{ ... }` and don't require the condition to be enclosed in
parentheses.  Other than that they behave like in C.

### let

```c
let x = 42;
let y;
```

The first form declares declares an `x` binding to a variable with a sum-type `{integer}` and a value of 42.  Sum-types
will not be discussed in this chapter, know however that they work like magic and make programming fun.  They bring the
`inferred` into the `statically-typed inferred language` of `&`.  (Fun fact: one-fifth, that's 20%, of all compiler code
is dedicated to type inference and sum-types.)

The second form declares a binding `y` with a sum-type `{any}`.  `y` cannot be used properly until it is somehow
constrained, for example by applying it to a function with a known parameter type (this currently holds for every
function parameter) or by assigning a value to it.  The precise rules for how `y` can be used before it is constrained
will not be discussed in this chapter.

An alternative way to declare variables is to use their types along with `let`, like so:

```c
let x: sint;
let y: sint = 42;
```

This makes type-checking easier and is sometimes useful when wanting to use undefined values.

### match

`match` is a magical statement that makes the workflow pleasant and beautiful.  NOTE: `match` is not finished and
isn't yet magical, therefore it doesn't make the workflow very pleasant and beautiful yet.

The general form is:

```c
match value {
    ... cases
}
```

Where cases is a list of pattern-statement pairs.  A pattern can be any of: variant destructuring, record destructuring,
binding, the keyword `else`.  Let's see some different usage cases of `match`

```c
tagged Tagged {
    Integer {
        int: sint,
    },
    Float {
        real: float,
    },
}

fn print: (value: Tagged) -> sint {
    match value {
        Tagged::Integer(int) => return printf("%d".ptr, int);
        Tagged::Float(real) => return printf("%f".ptr, cast(double, real));
        else => return -1;
    }
}

fn update: (ref: *Tagged, newvalue: sint) {
    match (ref) {
        Tagged::Integer(int) => *int = newvalue;
        Tagged::Float(real) => *real = cast(float, newvalue);
    }
}
```

Let's review the first one.  There we match `value` by value, which makes the bindings `.int` and `.real` bound to the
values of the corresponding fields.  `else` is self-explanatory.

The second `match` statements matches by reference, which makes the bindings `.int` and `.real` into pointers to the
corresponding fields.  This allows one to update the value inside the `tagged`.

`match` can also be used to match over integers and enums.  This works similar to `switch` in C, with the exception that
there is no fallthrough.

```c
enum Enum {
    Foo,
    Bar,
    Baz = 255,
}

// ...

    let value = Enum::Foo;
    match (value) {
        Enum::Foo => { ... } // do one thing
        Enum::Bar => { ... } // do another thing
        Enum::Baz => { ... } // do yet thing
        else => { ... } // value was created as a semantically valid, but formally invalid Enum instance
    }
```

## foreach

The foreach statement is actually just syntactical sugar for a macro application (more on macros in a later chapter).  A
foreach statement expands to an inline definition of some repetition.  The general form is:

```
foreach it in iterator {
    // do something
}
```

For example:

```
foreach i in 0 .. 10 {
    printf("%zd\n", i);
}
```

## Functions and overloading

In `&` functions are bound with their parameters.  This leads to the ability to define multiple functions with the same
name in the same module.

```c
fn add: (a: sint, b: sint) -> sint {
    return a + b;
}

fn add: (a: float, b: float) -> float {
    return a + b;
}
```

The correct implementation will be found when applying some arguments to the function, like so:

```c
add(1, 2);
add(1.0, 2.0);
```

### declare

A function can also be declared without providing its body.  This is useful when a function is defined outside of
ampersand (otherwise it should be imported as described in [Modules](#modules)), or when a C header is not available.  In
`declare`, function parameters need not be named.

```c
fn printf: (fmt: *schar, ...) -> sint;
```

## Static, extern and define

There are three ways to define top-level, i.e. global, "variables".  The word variable is quoted, because globals
defined through the word `define` are compile time constants.  These work like `#define` in C, but are more advanced,
statically typed and are not implemented by a simple textual replacement.

`static` can be used to define a mutable or immutable global variable.  Its other variant `static extern` is used to
define a global variable whose definition is provided by a different translation unit (possibly in another language,
like C).

Let's see all of these in a simple example.

```c
// global, mutable, link-time variable
static COUNTER: sint = 0;
// file-local, mutable, link-time variable
#[local]
static COUNTER_INIT: sint = 0;

// thread-local, mutable, link-time variable
#[thread]
static MAX_VALUE: float = 0.0;

// global, immutable, external, link-time variable
static extern stdout: *FILE;

// global, immutable, compile-time constant
define SINT_SIZE: usizet = 4;
```

## Operator overloading

Most operators can be overloaded in `&`.  They are overloaded similarly to how one would overload operators in Julia.

To overload the `+` operator, one would would define an `__add__` function with the desired arguments.

```c
struct Vec2 {
    x: float,
    y: float,
}

fn __add__: (Vec2 a, Vec2 b) -> Vec2 {
    return Vec2(x: a.x + b.x, y: a.y + b.y);
}

fn main: () -> sint {
    let u = Vec2(x: 1.0, y: 2.0);
    let v = u + u;
}
```

The table for overloadable operator and their corresponding functions is listed below.

| Operator   | Function |
| ---------- | -------- |
| `*`        | `__mul__` |
| `/`        | `__div__` |
| `%`        | `__rem__` |
| `+`        | `__add__` |
| `-` binary | `__sub__` |
| `-` unary  | `__neg__` |
| `++`       | `__inc__` |
| `--`       | `__dec__` |
| `?`        | `__ask__` |
| `!`        | `__not__` |
| `&`        | `__bitand__` |
| `\|`       | `__bitor__` |
| `^`        | `__bitxor__` |
| `~`        | `__bitnot__` |
| `<<`       | `__shl__` |
| `>>`       | `__shr__` |
| `[]`       | `__index__` |
| `*=`       | `__assign_mul__` |
| `/=`       | `__assign_div__` |
| `%=`       | `__assign_rem__` |
| `+=`       | `__assign_add__` |
| `-=`       | `__assign_sub__` |
| `&=`       | `__assign_bitand__` |
| `\|=`      | `__assign_bitor__` |
| `^=`       | `__assign_bitxor__` |
| `<<=`      | `__assign_shl__` |
| `>>=`      | `__assign_shr__` |

Remarks: The `ask` operator is used whenever the core language requires a boolean, so in the ternary operator `?:`,
`if`, `while`, `do while` and `for` statements.

Additionaly, some operators can be used even when they're not implemented explicitly.  The `__not__` operator has a
default implementation for any type which implements `__ask__`.  The various `__assign_*__` operators have a default
implementation if their left-hand side implements the non-assign variant of the operator.

NOTE: The usage of overloaded assign operators is broken and shouldn't be used.

Special requirements: The ask and not operators must return booleans.  If the index operator returns a pointer, it will
be dereferenced automatically upon application; this leads to the following behaviour being possible: `a[i] = 0`.  The
assign operators accept a pointer as their first parameter.

## Parametric polymorphism

In many contexts a top-level definition can be parameterized.  This holds for structs, unions, tagged unions, and
functions.  Here's a real world example to showcase that.

```c
struct Vec3 {
    T: type
    x: T,
    y: T,
    z: T,
}

fn __neg__: (T: type, a: Vec3(T: T)) -> Vec3(T: T) {
    return Vec3(x: -a.x, y: -a.y, z: -a.z);
}

fn __add__: (T: type, a: Vec3(T: T), b: Vec3(T: T)) -> Vec3(T: T) {
    return Vec3(x: a.x + b.x, y: a.y + b.y, z: a.z + b.z);
}

fn __sub__: (T: type, a: Vec3(T: T), b: Vec3(T: T)) -> Vec3(T: T) {
    return Vec3(x: a.x - b.x, y: a.y - b.y, z: a.z - b.z);
}

fn __mul__: (T: type, a: Vec3(T: T), b: Vec3(T: T)) -> Vec3(T: T) {
    return Vec3(x: a.x * b.x, y: a.y * b.y, z: a.z * b.z);
}

fn __div__: (T: type, a: Vec3(T: T), b: Vec3(T: T)) -> Vec3(T: T) {
    return Vec3(x: a.x / b.x, y: a.y / b.y, z: a.z / b.z);
}
```

## Modules

In larger project you might want to use multiple files to split up your definitions across modules.  In ampersand &
there's the notion of a file, or a translation unit, and that of a module, or a namespace.

The keyword `mod` is used to declare a file-local namespace.  A single namespace can be split across various files, but
only one namespace can be used in a single file.  Definitions inside that file can be referred to without the module
prefix.

```c
// in src/module.amp
mod example::module;

fn select: (p: bool, a: sint, b: sint) -> sint {
    return p? a : b;
}

fn run: () -> unit {
    printf("%d\n".ptr, select(true, 1, 2));
}
```

Files can be imported using the `use` keyword.  This will import all the definitions in that file under the namespace
they were declared in.  For example:

```c
// in src/module.amp
mod example::module;

#[local]
fn select: (p: bool, a: sint, b: sint) -> sint {
    return p? a : b;
}

fn run: () -> unit {
    printf("%d\n".ptr, select(true, 1, 2));
}

// in src/main.amp
use module;

#[no_mangle]
fn main: () -> sint {
    example::module::run();
    return 0;
}
```

Note that we had to use the prefix `example::module::` to execute the function `run`.  The function `select` cannot be
called from `src/main.amp` at all, because it was declared with the attribute `local`.  Any definition declared with
that attribute will be visible only to that file (not the module, but the file).  Note also, that although the file
`module.amp` is located in the directory `src/`, it can be imported using just the declaration `use module;`.  That is
because files are searched for in the compiler specific PATH list.  This PATH automatically includes a) the current
directory and b) the parent directory of every file that is being compiled.  This may lead to ambiguities, for example
in the following scheme, so it is advised to always use the full path omitting the source directory.  In the following
scheme, if `main.amp` includes `use module;`, it would import one of the two modules and it is unspecified which one.
So instead use `use a::module;` to avoid such ambiguities.

```
$ etc -o=main src/main.amp src/a/module.amp src/b/module.amp
$ tree
./
|- src/
   |- main.amp
   |- a/
   |  |- module.amp
   |- b/
      |- module.amp
```

Remember how we had to use `example::module::run()` to execute the function `run`?  This would get quite tedious if we
had long module definitions.  So instead one might import one or all the definitions from a module into the current
module using the `with` top-level definition.  `with` has to be used after the `use` that it imports from.


```c
// in src/module.amp
mod example::module;

#[local]
fn select: (p: bool, a: sint, b: sint) -> sint {
    return p? a : b;
}

fn run: () -> unit {
    printf("%d\n".ptr, select(true, 1, 2));
}

// in src/main.amp
use module;

with example::module::select;
// alternatively:
with example::module::*;

#[no_mangle]
fn main: () -> sint {
    run();
    return 0;
}
```

### use extern

The top-level definition `use extern` is a bit magical.  It can be used to import a file from another language.
Currently the only language supported is C and no other languages are planned.  Every external top-level definition will
be imported from a C header when imported with `use extern "C" "header.h"`.  One exception is macros --- macros will not
be imported currently.

# What's next?

Next up, you should read the [Good practices](./good-practices.md) section of the guide.  It covers idioms and good
practices which help you write secure code.

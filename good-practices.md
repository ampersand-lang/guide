# Good practices

## Idioms

### `new`, `default`, `copy`, `clone` and `del`

These 5 functions should be defined for most custom types.  Here are their signatures and uses listed.

- `new` doesn't have a set signature, but it starts with `void new(*T, ...)`, where `...` represents the additional
parameters that might be used to create a new datatype.  `*T` represent the storage in which the new value should be
created.  The reason why the signature isn't `T new(...)` is that the type inference system isn't able to deduce the
type of `new()` if it is overloaded (which it is).
- `default` has the signature `void default(*T)`.  It should be provided if a formally correct default value exists for
the type.  Other than that it's similar to `new`.
- `copy` is a cheap copy operation.  It shouldn't perform any deep cloning, but it should produce a formally correct
unique value.  This means for example, that a simple `memcpy` isn't valid for copying arrays, because the values
underneath might not be bitwise-copyable. The signature must be `T copy(T)`.  `copy` isn't valid for every type, for
example it is not valid for the `std::box::Box` type, which by definition requires a deep clone.
- `clone` is a deep, expensive copy operatino.  It should create a deep copy of the entire state, allowing only
references (shared or otherwise) to be shared between the copies.  The signature is the same as that of `copy`: `T
clone(T)`.
- `del` is the delete operation.  It should free and cleanup all owned/unique resources, like owned smart pointers,
owned dynamic arrays, owned system resources (e.g. files and streams), mmaps, etc.

### `write` and `debug`

Most custom types should define the `debug` function, which prints a representation of the type in a standardized
format.  The format is:

- for numbers; standard decimal representation: `1234`, `123.4`.
- for strings: `"String \" <- escaped quotation, escaped ascii character -> \x0a"`
- for characters: `'a'`, `'\''`, `'\x0a'`
- for structs: `Struct(field1: "value", field2: "value")`
- for unions: `Union(0x12, 0x34)`
- for enums: `Enum::VARIANT`
- for tagged: `Tagged::Variant(field1: "value", field2: "value")`
- for list-like types: `[1, 2, 3, 4, 5]`
- for set-like types: `{ 1, 2, 3, 4 }`
- for map-like types: `{ "a": 1, "b": 2, "c": 3, "d": 4 }`

Since it is impossible to tell the contents of a union, the only way to print its contents is to print the raw bytes (in
hex).  

If a struct, union, or variant doesn't have any fields, the entire `{}` can be omitted.

The signature of `debug` should be:

```
fn debug: (out: *W, t: T) -> usizet;
```

This function is polymorphic. `W*` is the output stream.  The standard library currently only expects one of two types
to be used as `W`, the C file handle `FILE` or the `std`-defined dynamic string `std::dstr::Dstring`.  `T` is the type
to be printed.  `debug` shall return the number of bytes that were printed.

`debug` is defined for all `std` types.

Similarly a `write` function exists, which writes a human-readable representation of the datatype.  This is completely
implementation defined and not standardized in the slightest.  This representation can be anything at all.  Only the
builtin primitive types and `String` implement this function in the standard library.

### `update` and `hash`

The standard library provides a non-cryptographic, efficient, 64-bit hashing function based on Jenkins hash.  To use it
one first has to create a Hasher:

```
let h: Hasher;
new(&h);
```

The hasher can then be updated using the `update` function:

```
let k = "mykey";
update(&h, k);
```

To get the result of a hasher one would use the finish function:

```
let hash_code = finish(&h);
```

Alternatively the `hash` function can be used to calculate the hash of an value, whose type implements `update`, inline.

```
let hash_code = hash("mykey");
```

Any type which might be used as a key in a hash map or a hash set should implement the `update` function.  It can be
implemented as follows:

```
tagged Foo {
    Bar {
        x: sint,
        y: sint,
    },
    Baz {
        x: slong,
        y: slong,
    },
}

fn update: (Hasher* h, Foo f) -> unit {
    update(h, f.tag);
    match f {
        Foo::Bar(x, y) => {
            update(h, x);
            update(h, y);
        }
        Foo::Baz(x, y) => {
            update(h, x);
            update(h, y);
        }
    }
}
```

Most primitive types and standard types implement `update`, therefore it is relatively easy to implement `update` for
custom types.

## ampstd

### The `Box` smart pointer

`Box` is a smart pointer in the sense that it carries some information with it, however it is purely compile-time data.
`Box` is literally defined as follows:

```
struct Box {
    T: type,
    ptr: *T,
}
```

`Box(T)` should be used for owned, or unique, references allocated on the heap.  This is similar to the `unique(T)` type
in C++ or the `Box(T)` type in Rust, without the move semantics.  It is however the programmers job to ensure, that no
two instances of the same `Box` exist at the same time - only one instance of a same pointer enclosed in a `Box` is
formally valid.

`new(&box, value)` will allocate a new value on the heap and put `value` inside of it, `del(box)` will call `del` on the
inner value and deallocate the memory used by `T`.

For more information check out the generated documentation.

### The `Ref` smart pointer

`Ref` is another compile-time smart pointer provided by the standard library.  It is however the exact opposite.  It
represents an arbitrary (heap, stack, or static) pointer to some shared, mutable data.  It is the programmers job to
ensure that correct mutability semantics are used - i.e. no two entities (for example threads) are modifying the
underlying value at the same time.

`new(&ref, ptr_to_value)` will wrap the pointer to a value in a new `Ref(T)`.  Additionally, `as_ref(box)` or
`as_ref(ptr)d can be used to quickly create a `Ref(T)` from a `Box(T)`.  `del(ref)` is a noop.  Both `clone` and `copy`
are identity functions for `Ref(T)`.

`Ref(T)` provides semantics which more closely match that of a value, instead of a pointer.  For example, comparing two
`Ref`s compares their underlying values instead of the pointer.

### The difference between `Box(T)`, `Ref(T)` and `*T`

When do I use each of these three pointer types?  The answer is simple.

Use `Box(T)` when you need to store something on the heap; when you need to tell the program that a pointer is owned,
i.e. it is unique and no other entity owns the value behind it; when you need a central storage for a value which is
referenced by multiple entities.

Use `Ref(T)` when you need a shared, unchecked pointer to a heap, stack or static variable; when you need to tell the
program that you don't own a pointer, i.e. you don't have the right to `del` the value underneath the `Ref`; when you
need value-like semantics for a pointer-like type.

Use `*T` when you need a shared, unchecked pointer to a heap, stack or static variable; when you need to tell the
program that you don't own a pointer, i.e. you don't have the right to `del` the value underneath the `*`; when you need
pointer semantics for a pointer-like type, for example when you need to do raw hardware access, or need to perform
pointer arithmetic.

### `Array(T)` and `Dstring`

`Array` is the `std`-defined growable, or dynamic, array.  Its semantics and characteristics are similar to what's
called a `vec` in C++, or `Vec` in Rust.  It is a contigous, `O(n)`-growable (`O(1)` amortized),
`O(1)`-accessible dynamic array.  The `__index__` operator is overloaded for `Array`s, allowing one to access its
elements directly.  Arrays can be indexed with a `usizet` or with a `Range(usizet)`, yielding a slice.  Indexing is not
bounds-checked.  The length can be checked with the `len(array)` function.  The array owns its resources, meaning that
they will be deleted once the whole array is deleted.

`Dstring` is a wrapper around `Array(T: schar)` that includes some helper functions for dealing with strings specifically.

### `Option(T)` and `Result(T, E)`

These two types mimic the Haskell `Maybe a` and `Either l r`, or the Rust `Option<T>` and `Result<T, E>` types.  They
provide a standard way to declare the existence or non-existence of a value (`Option(T)`) and either one of two results;
a positive and negative result (`Result(T, E)`).  An option may be declared either as a `Option::Some(some)` or a
`Option::None()`.  A result may be declared either as a `Result::Ok(ok)` or a `Result::Err(err)`.

### `HashMap(K, V)` and `HashSet(K)`

The `&` standard library also includes an implementation of a hash map with `O(ln n)` (but practically constant) inserts,
lookups and removal operations and a hash set with the same characteristics.  These are implemented through hash array
mapped tries similar to those provided by Clojure, but they are mutable, as opposed to the persistent data structures
present in Clojure.  ([Ideal Hash Trees - Bagwell, 2000](https://infoscience.epfl.ch/record/64398/files/idealhashtrees.pdf))

### `Deque(T)`

Another standard-provided is the double-ended queue, or deque for short.  It is a collection type with a contigous
storage characterized by constant time pushes to the back of the queue, constant time pops from the front of the queue,
constant time element access.  Currently the implementation doesn't provide a `insert` or `remove` function - modifying
the buffer is only possible through `push_back` and `pop_front`.
